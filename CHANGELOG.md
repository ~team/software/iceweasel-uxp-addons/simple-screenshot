# Changelog

### 1.3

* Added support for custom filename template.
* Fixed file names of edited screenshots.
* Fixed inconsistent file names of page region captures.

### 1.2.1

* Fixed default extension preferences prefix.

### 1.2

* Added preferences for default save path and filename template.

### 1.1.1

* Fixed checking if button exists before trying to disable it.

### 1.1

* Adjusted toolbar button and icons.
* Added operations to context menu.

### 1.0

* Initial release.
