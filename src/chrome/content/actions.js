var EXPORTED_SYMBOLS = ['saveScreenshotToDisk', 'copyScreenshotToClipboard'];

var Screenshoter = {
    pref: Components.classes["@mozilla.org/preferences-service;1"]
                    .getService(Components.interfaces.nsIPrefService)
                    .getBranch("extensions.simplescreenshot.")
}

var saveScreenshotToDisk = function(canvas, title, domain) {
    var { classes: Cc, interfaces: Ci } = Components;
    var _strings = Cc["@mozilla.org/intl/stringbundle;1"].getService(Ci.nsIStringBundleService)
                                                         .createBundle("chrome://simplescreenshot/locale/simplescreenshot.properties");

    var getDefaultFolder = function() {
        var file = null;
        try {
            file = Components.classes["@mozilla.org/file/directory_service;1"]
	                     .getService(Components.interfaces.nsIProperties)
                             .get("Desk", Components.interfaces.nsIFile);
        } catch (e) {
            try {
                file = Components.classes["@mozilla.org/file/directory_service;1"]
                                 .getService(Components.interfaces.nsIProperties)
                                 .get("TmpD", Components.interfaces.nsIFile);
            } catch (e) {
            }
        }
        return file;
    };

    var getSaveFolder = function() {
        var fileName = Screenshoter.pref.getComplexValue("defaultFolder",
            Components.interfaces.nsISupportsString).data;
        if (fileName == null || fileName.length == 0) {
            return getDefaultFolder();
        }

        var file = Components.classes["@mozilla.org/file/local;1"]
	                     .createInstance(Components.interfaces.nsILocalFile);
        file.initWithPath(fileName);
        if (!file.exists() || !file.isDirectory() || !file.isWritable()) {
            alert("Target folder is not valid, saving to desktop.")
            return getDefaultFolder();
        }
        return file;
    };

    var getOutputFilename = function(canvas) {
        if ((typeof title === "undefined") || !title) {
            title = "screen";
        }

        // Replace invalid characters  \ / : * ? ' " < > |  with underscore
        var normalizedTitle = title.replace(/[\/\\\:\*\?\'\"\<\>\|\s]/g, '_').replace(/_+/g, '_');

        var d = new Date();

        var year = d.getFullYear();
        var month = ("0" + (d.getMonth()+1)).slice(-2);
        var day = ("0" +  d.getDate()    ).slice(-2)

        var hours = ("0" +  d.getHours()   ).slice(-2);
        var minutes = ("0" +  d.getMinutes() ).slice(-2);
        var seconds = ("0" +  d.getSeconds() ).slice(-2);

        var dateString = [year, month, day].join("-");
        var timeString = [hours, minutes, seconds].join("-");

        var template = Screenshoter.pref.getCharPref("filenameTemplate");
        if (template == 'title') {
            name = normalizedTitle + "_" + dateString + "_" + timeString;
        } else if (template == 'domain') {
            name = domain + "_" + dateString + "_" + timeString;
        } else {
            var customTemplate = Screenshoter.pref.getCharPref("filenameTemplateCustom");
            name = customTemplate.replace("%N", normalizedTitle)
                                 .replace("%T", title)
                                 .replace("%D", domain)
                                 .replace("%Y", year)
                                 .replace("%m", month)
                                 .replace("%d", day)
                                 .replace("%H", hours)
                                 .replace("%M", minutes)
                                 .replace("%S", seconds);
        }

        return name;
    };

    var fp = Cc['@mozilla.org/filepicker;1'].createInstance(Ci.nsIFilePicker);
    fp.init(canvas.ownerDocument.defaultView.parent, _strings.GetStringFromName('saveImageTo'), Ci.nsIFilePicker.modeSave);
    var saveTarget = getSaveFolder();
    saveTarget.append(getOutputFilename(canvas) + '.png');
    fp.defaultString = saveTarget.path;
    fp.appendFilter(_strings.GetStringFromName('pngImage'), '*.png');

    if (fp.show() != Ci.nsIFilePicker.returnCancel) {
        var file = Cc['@mozilla.org/file/local;1'].createInstance(Ci.nsILocalFile);
        var path = fp.file.path;
        file.initWithPath(path + (/\.png$/.test(path) ? '' : '.png'));

        var ios = Cc['@mozilla.org/network/io-service;1'].getService(Ci.nsIIOService);
        var data = canvas.toDataURL("image/png", "");
        var source = ios.newURI(data, 'utf8', null);
        var target = ios.newFileURI(file);

        var persist = Cc['@mozilla.org/embedding/browser/nsWebBrowserPersist;1'].createInstance(Ci.nsIWebBrowserPersist);
        persist.persistFlags = Ci.nsIWebBrowserPersist.PERSIST_FLAGS_AUTODETECT_APPLY_CONVERSION;

        var transfer = Cc['@mozilla.org/transfer;1'].createInstance(Ci.nsITransfer);
        transfer.init(source, target, '', null, null, null, persist, false);
        persist.progressListener = transfer;

        persist.saveURI(source, null, null, null, null, null, file, null);
        return true;
    }
    return false;
};


/*
var copyScreenshotToClipboard = function(canvas) {
    var consoleService = Components.classes["@mozilla.org/consoleservice;1"].getService(Components.interfaces.nsIConsoleService);


    const nsSupportsString = Components.Constructor('@mozilla.org/supports-cstring;1',
                                                    'nsISupportsCString');
    const String = function(value) {
        let res = nsSupportsCString();
        res.data = value;
        return res;
    };

    const nsSupportsVoid = Components.Constructor('@mozilla.org/supports-void;1',
                                                  'nsISupportsVoid');

    // var transferable = Components.classes["@mozilla.org/widget/transferable;1"]
    //                             .createInstance(Components.interfaces.nsITransferable);

    // var data = canvas.toDataURL("image/png", "");
    // consoleService.logStringMessage(data);
    // consoleService.logStringMessage(data.length);

    // transferable.addDataFlavor('image/png');
    // transferable.setTransferData('image/png', String(data), data.length);

    // var clipboard = Components.classes["@mozilla.org/widget/clipboard;1"]
    //                           .getService(Components.interfaces.nsIClipboard);

    // clipboard.setData(transferable, null, clipboard.kGlobalClipboard);

    canvas.toBlob(
        function(blob) {
            const reader = new FileReader();
            reader.addEventListener('loadend', () => {
                    consoleService.logStringMessage(blob.size);
                    consoleService.logStringMessage(reader.result.byteLength);
                    var transferable = Components.classes["@mozilla.org/widget/transferable;1"]
                                                 .createInstance(Components.interfaces.nsITransferable);
                    transferable.addDataFlavor('image/png');
                    transferable.setTransferData('image/png', nsSupportsVoid(reader.result), reader.result.byteLength);

                    var clipboard = Components.classes["@mozilla.org/widget/clipboard;1"]
                                              .getService(Components.interfaces.nsIClipboard);
                    clipboard.setData(transferable, null, clipboard.kGlobalClipboard);
                });
            reader.readAsArrayBuffer(blob);
        },
        "image/png"
    );
};
*/
