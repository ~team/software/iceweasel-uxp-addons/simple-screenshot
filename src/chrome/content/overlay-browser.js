/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

Components.utils.import("resource://simplescreenshot/snapshot.js");
Components.utils.import("chrome://simplescreenshot/content/actions.js");

var SimpleScreenshot = {
    handleEvent: function(aEvent) {
        switch (aEvent.type) {
            case "load":
                setTimeout(this.init.bind(this), 500);
                break;
            case "unload":
                this.uninit();
                break;
            case "TabSelect":
            case "DOMContentLoaded":
                this.updateUI(this.isHttp());
                break;
        }
    },

    init: function() {
        gBrowser.tabContainer.addEventListener("TabSelect", this, false);
        window.addEventListener("DOMContentLoaded", this, false);
    },

    uninit: function() {
        window.removeEventListener("DOMContentLoaded", this, false);
    },

    isHttp: function() {
        var tab = gBrowser.mCurrentTab;
        var uri = null;
        if (tab && tab.linkedBrowser) {
            uri = tab.linkedBrowser.currentURI;
        }
        return (uri && (uri.scheme == "http" || uri.scheme == "https"));
    },

    updateUI: function(isHttp) {
        var btn =  document.getElementById("ce_simplescreenshot");
        // var select = document.getElementById("simplescreenshot-snapshot-select");
        // var entire = document.getElementById("simplescreenshot-snapshot-entire");
        if (this.isHttp()) {
            if (btn) {
                btn.removeAttribute("disabled");
            }
            // select.setAttribute("disabled", "false");
            // entire.setAttribute("disabled", "false");
        } else {
            if (btn) {
                btn.setAttribute("disabled", "true");
            }
            // select.setAttribute("disabled", "true");
            // entire.setAttribute("disabled", "true");
        }
    },

    getScreenshot: function(action, part, data) {
        var contentWindow = window.content;
        var document = contentWindow.document;

        if (part == 'data') {
            data.canvas.ownerDocument.title = document.title;
            data.canvas.ownerDocument.domain = document.domain;
            return SimpleScreenshot.sendScreenshot(action, data.canvas, data.ctx);
        }

        var width, height, x, y;
        switch (part) {
            case 'visible':
                x = document.documentElement.scrollLeft;
                y = document.documentElement.scrollTop;
                width = document.documentElement.clientWidth;
                height = document.documentElement.clientHeight;
                break;
            case 'entire':
                x = y = 0;
                width = Math.max(document.documentElement.scrollWidth, document.body.scrollWidth);
                height = Math.max(document.documentElement.scrollHeight, document.body.scrollHeight);
                break;
            default:
                break;
        }

        var canvas = null;
        try {
            canvas = document.createElementNS("http://www.w3.org/1999/xhtml", "html:canvas");
            canvas.height = height;
            canvas.width = width;

            // maybe https://bugzil.la/729026#c10 ?
            var ctx = canvas.getContext("2d");

            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.save();
            ctx.drawWindow(contentWindow, x, y, width, height, "rgb(255,255,255)");
        } catch (err) {
            canvas = null;
            canvas = document.createElementNS("http://www.w3.org/1999/xhtml", "html:canvas");
            var scale = Math.min(1, Math.min(8192 / height, 8192 / width));
            canvas.height = height * scale;
            canvas.width = width * scale;

            var ctx = canvas.getContext("2d");

            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.scale(scale, scale);
            ctx.save();
            ctx.drawWindow(contentWindow, x, y, width, height, "rgb(255,255,255)");
        }

        canvas.ownerDocument.title = document.title;
        canvas.ownerDocument.domain = document.domain;
        SimpleScreenshot.sendScreenshot(action, canvas, ctx);
    },

    sendScreenshot: function(action, canvas, ctx) {
        switch (action) {
            case "local":
                saveScreenshotToDisk(canvas, canvas.ownerDocument.title, canvas.ownerDocument.domain);
                break;
            case "editor":
                ScreenshotStorage.push({
                    "data": ctx.getImageData(0, 0, canvas.width, canvas.height),
                    "title": canvas.ownerDocument.title,
                    "domain": canvas.ownerDocument.domain,
                });
                openUILinkIn("chrome://simplescreenshot/content/editor.xhtml", "tab");
                break;
            // case "clipboard":
            //     copyScreenshotToClipboard(canvas);
            //     break;
        }
    },
};
