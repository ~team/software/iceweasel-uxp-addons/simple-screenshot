Simple Screenshot lets you capture a web page as an image.
It can capture the whole page, the visible portion of the page,
or the region that you select.
You can do some basic editing on the screenshot before saving it,
such as drawing lines and circles, adding text, blurring some parts,
and cropping.

Simple Screenshot is a fork of the Easy Screenshot extension by Mozilla Online.
